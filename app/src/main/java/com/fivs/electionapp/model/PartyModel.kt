package com.fivs.electionadminapp.data.model

import java.io.Serializable

open class PartyModel:Serializable {
    var id : String = ""
    var image : String = ""
    var name : String = ""
    var state : String = ""
    var stateid : String = ""
    var isRegional : String = ""
}