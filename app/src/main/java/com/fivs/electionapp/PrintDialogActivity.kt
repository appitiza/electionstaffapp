package com.fivs.electionapp

import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import androidx.appcompat.app.AppCompatActivity


class PrintDialogActivity : AppCompatActivity() {
    private val PRINT_DIALOG_URL = "https://www.google.com/cloudprint/dialog.html"
    private val JS_INTERFACE = "AndroidPrintDialog"
    private val CONTENT_TRANSFER_ENCODING = "base64"

    private val ZXING_URL = "http://zxing.appspot.com"
    private val ZXING_SCAN_REQUEST = 65743

    /**
     * Post message that is sent by Print Dialog web page when the printing dialog
     * needs to be closed.
     */
    private val CLOSE_POST_MESSAGE_NAME = "cp-dialog-on-close"

    /**
     * Web view element to show the printing dialog in.
     */
    private val dialogWebView: WebView? = null

    /**
     * Intent that started the action.
     */
    var cloudPrintIntent: Intent? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_print_dialog)
    }
}
