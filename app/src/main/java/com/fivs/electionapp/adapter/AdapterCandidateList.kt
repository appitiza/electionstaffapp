package com.fivs.electionapp.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.fivs.electionadminapp.data.model.CandidateModel
import com.fivs.electionapp.R
import com.fivs.electionapp.callback.CandidateClickCallback
import kotlinx.android.synthetic.main.item_candidate_list.view.*
import okhttp3.internal.notify
import okhttp3.internal.notifyAll

class AdapterCandidateList(
    var context: Context,
    private var callback: CandidateClickCallback,
    var mList: ArrayList<CandidateModel>
) :
    androidx.recyclerview.widget.RecyclerView.Adapter<AdapterCandidateList.ViewHolder>() {


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, callback, mList[position])
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_candidate_list, parent, false)
        return ViewHolder(view)
    }


    class ViewHolder(itemView: View) :
        androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {


        fun onBind(
            context: Context,
            callback: CandidateClickCallback,
            data: CandidateModel
        ) {


            var requestOptions1 = RequestOptions()
            requestOptions1 =
                requestOptions1.circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)

            Glide.with(context)
                .load(data.image)
                .apply(requestOptions1)
                .into(itemView.ivCandidateImage)


            var requestOptions2 = RequestOptions()
            requestOptions2 =
                requestOptions2.circleCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.image_placeholder)
                    .placeholder(R.drawable.image_placeholder)

            if (data.party == "other") {
                Glide.with(context)
                    .load(data.logo)
                    .apply(requestOptions2)
                    .into(itemView.ivCandidateLogo)
            } else {
                Glide.with(context)
                    .load(data.partyimage)
                    .apply(requestOptions2)
                    .into(itemView.ivCandidateLogo)
            }

            if(data.mIsSelected)
            {
                itemView.root.setBackgroundColor(Color.parseColor("#2196F3"))
            }
            else
            {
                itemView.root.setBackgroundColor(Color.parseColor("#FFFFFF"))
            }
            itemView.tvName.text = data.name
            itemView.tvConstituency.text = data.constituency
            itemView.tvState.text = data.state
            itemView.tvParty.text = data.party
            itemView.root.setOnClickListener {

                callback.onItemClick(position = adapterPosition)

            }
            itemView.ivCandidateImage.setOnClickListener {

                callback.onImageClick(position = adapterPosition)

            }

        }
    }

}