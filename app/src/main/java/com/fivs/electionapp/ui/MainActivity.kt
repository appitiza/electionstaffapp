package com.fivs.electionapp.ui

import android.content.Intent
import android.os.Bundle
import android.webkit.WebView
import com.fivs.electionapp.R
import com.fivs.electionapp.model.ConstituencyModel
import com.fivs.electionapp.model.StaffModel
import com.fivs.electionapp.utils.Constants
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*


class MainActivity : BaseActivity() {

    private var mStaffModel = StaffModel()
    private var mConstituencyModel = ConstituencyModel()
    private var mWebView: WebView? = null
    private var isElectionBlocked = ""
    private var isProcessing = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setScreenTitle(R.string.app_name)
        getIntentData()
        setClick()

    }

    private fun setClick() {
        btnVotting.setOnClickListener {
            if (isNetworkAvailable()) {
                getElectionTime()
            } else {
                showMessage(this, getString(R.string.no_network))
            }
        }
        btnConstituencyStatus.setOnClickListener {
            if (isNetworkAvailable()) {
                val intent = Intent(this, ConstituencyDetailsActivity::class.java)
                val mBundle = Bundle()
                mBundle.putSerializable("staffdata", mStaffModel)
                intent.putExtras(mBundle)
                startActivity(intent)
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
    }

    private fun getIntentData() {
        if (intent.extras != null && intent.extras!!.getSerializable("staffdata") != null) {
            mStaffModel = (intent.extras!!.getSerializable("staffdata") as StaffModel?)!!
            getConstituencyInfo(mStaffModel.constituencyid, true)

        }
    }

    private fun getConstituencyInfo(id: String, isFirst: Boolean) {

        showProgressDialog(getString(R.string.loading))
        isProcessing = true
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_CONSTITUENCY)
        val query = ref
            .document(id)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if (fetchall_task.exists()) {
                    if (fetchall_task != null) {
                        val mCategoryData =
                            ConstituencyModel()
                        mCategoryData.id = fetchall_task.id
                        mCategoryData.image =
                            (fetchall_task?.get(Constants.CONSTITUENCY_IMAGE)).toString()
                        mCategoryData.name =
                            (fetchall_task.get(Constants.CONSTITUENCY_NAME)).toString()
                        mCategoryData.isInactive =
                            (fetchall_task.get(Constants.CONSTITUENCY_STATUS)).toString()

                        mCategoryData.cast_vote =
                            (fetchall_task.get(Constants.CONSTITUENCY_CAST_VOTE)).toString().toInt()
                        mCategoryData.voter_count =
                            (fetchall_task.get(Constants.CONSTITUENCY_VOTER_COUNT)).toString()
                                .toInt()
                        mCategoryData.won_candidate =
                            (fetchall_task.get(Constants.CONSTITUENCY_WON_CANDIDATE)).toString()

                        mCategoryData.stateid =
                            (fetchall_task.get(Constants.STATE_ID)).toString()
                        mCategoryData.state = ""

                        mConstituencyModel = mCategoryData

                        val displayString = StringBuilder()
                        displayString.append("Logged In As: ")
                        displayString.append(mStaffModel.name)
                        displayString.append("\n")
                        displayString.append("Logged In To: ")
                        displayString.append(mConstituencyModel.name)
                        displayString.append("\n")
                        displayString.append("Total Voter Count: ")
                        displayString.append(mConstituencyModel.voter_count)
                        tvInfo.text = displayString.toString()

                        if (mConstituencyModel.isInactive == "0") {
                            if (!isFirst) {
                                getElectionStatus()
                            }
                        } else {
                            showMessage(this, getString(R.string.election_has_blocked))
                        }
                    }
                }
                isProcessing = false
                dismissProgressDialogPopup()
            }
            .addOnFailureListener {
                isProcessing = false
                dismissProgressDialogPopup()
            }

    }


    fun getElectionTime() {

        isProcessing = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val calendar = Calendar.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ELECTION_TIME)
        val query = ref
            .document(Constants.ELECTION_TIME_KEY)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if ((fetchall_task.data?.get(Constants.ELECTION_START_TIME)).toString() != "null") {
                    val parser =
                        SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyy", Locale.getDefault())
                    val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())


                    val start =
                        parser.parse(fetchall_task.data?.get(Constants.ELECTION_START_TIME).toString())
                    val end =
                        parser.parse(fetchall_task.data?.get(Constants.ELECTION_END_TIME).toString())
                    if (start!!.before(calendar.time) && end!!.after(calendar.time)) {
                        getConstituencyInfo(mConstituencyModel.id, false)
                    } else {
                        showMessage(this, getString(R.string.election_not_started))
                    }

                } else {
                    showMessage(this, getString(R.string.election_not_started))
                }

                isProcessing = false
                dismissProgressDialogPopup()

            }
            .addOnFailureListener {
                isProcessing = false
                dismissProgressDialogPopup()
            }
    }

    fun getElectionStatus() {

        isProcessing = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ELECTION_STATUS)
        val query = ref
            .document(Constants.ELECTION_STATUS_KEY)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if (fetchall_task.exists()) {
                    if ((fetchall_task.data?.get(Constants.ELECTION_BLOCKED)).toString() != "null") {
                        isElectionBlocked =
                            (fetchall_task.data?.get(Constants.ELECTION_BLOCKED)).toString()
                        if (isElectionBlocked.isNotEmpty()) {
                            if (isElectionBlocked == "0") {
                                val intent = Intent(this, EnterAdhaarActivity::class.java)
                                val mBundle = Bundle()
                                mBundle.putSerializable("staffdata", mStaffModel)
                                mBundle.putSerializable("constituencydata", mConstituencyModel)
                                intent.putExtras(mBundle)
                                startActivity(intent)
                            } else {

                                showMessage(this, getString(R.string.election_has_blocked))
                            }
                        } else {

                            showMessage(this, getString(R.string.election_has_blocked))
                        }
                    }
                    isProcessing = false
                    dismissProgressDialogPopup()
                } else {

                    showMessage(this, getString(R.string.election_has_blocked))
                }
            }
            .addOnFailureListener {
                isProcessing = false
                dismissProgressDialogPopup()
            }
    }

    override fun onBackPressed() {
        showMessage(this, getString(R.string.you_are_not_allowed))
    }
}
