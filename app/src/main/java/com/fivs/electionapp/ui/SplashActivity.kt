package com.fivs.electionapp.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.fivs.electionapp.R
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.*

class SplashActivity : AppCompatActivity() {

    private var mAuth: FirebaseAuth? = null
    private val delayTimeAfterApi: Long = 1500
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        initializeFirebase()
        delaySplashScreen()
    }

    private fun initializeFirebase() {
        mAuth = FirebaseAuth.getInstance()
    }

    private fun delaySplashScreen() = GlobalScope.launch {
        withContext(Dispatchers.Default) { delay(delayTimeAfterApi) }
        startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
        finish()


    }
}
