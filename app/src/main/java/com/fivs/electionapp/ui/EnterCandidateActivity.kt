package com.fivs.electionapp.ui

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.fivs.electionadminapp.data.model.CandidateModel
import com.fivs.electionadminapp.data.model.PartyModel
import com.fivs.electionapp.R
import com.fivs.electionapp.adapter.AdapterCandidateList
import com.fivs.electionapp.callback.CandidateClickCallback
import com.fivs.electionapp.model.ConstituencyModel
import com.fivs.electionapp.model.SmsResponseModel
import com.fivs.electionapp.model.StaffModel
import com.fivs.electionapp.model.VoterModel
import com.fivs.electionapp.network.APIClient
import com.fivs.electionapp.network.ApiInterface
import com.fivs.electionapp.utils.Constants
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.android.synthetic.main.activity_enter_candidate.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class EnterCandidateActivity : BaseActivity(), CandidateClickCallback {

    private lateinit var mAdapter: AdapterCandidateList

    private var mConstituencyModel = ConstituencyModel()
    private var mVoterModel = VoterModel()

    private var loading = false
    private var mCandidateList: ArrayList<CandidateModel> = arrayListOf()
    private var mDisplayedList: ArrayList<CandidateModel> = arrayListOf()
    private var mPartyDataList: ArrayList<PartyModel> = arrayListOf()
    private var candidate = CandidateModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_candidate)
        setScreenTitle(R.string.app_name)
        initializeComponents()
        getIntentData()
        setClick()


    }

    private fun initializeComponents() {
        rvList.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterCandidateList(this, this, mDisplayedList)
        rvList.adapter = mAdapter
    }

    private fun setClick() {
        btnProceed.setOnClickListener {
            if (isNetworkAvailable()) {
                castVote()
            } else {
                showMessage(this, getString(R.string.no_network))
            }
        }

    }

    private fun castVote() {
        if (mConstituencyModel.name != "" && mVoterModel.name != "" && mCandidateList.isNotEmpty()) {
            candidate = mCandidateList.filter { it.mIsSelected }.single()
            if (candidate != null) {
                updateVoter("1")
            }
        }
    }


    private fun updateVoter(type: String) {
        showProgressDialog(getString(R.string.voting))
        loading = true
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val map = HashMap<String, Any>()

        map[Constants.VOTER_CASTE_VOTE] = type

        map[Constants.DATE] = FieldValue.serverTimestamp()

        db!!.collection(Constants.COLLECTION_VOTER)
            .document(mVoterModel.id)
            .update(map)
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                loading = false
                if (reg_task.isSuccessful) {
                    if (type == "1") {
                        updateCandidate(1)
                    }
                } else {
                    updateVoter("0")
                    showMessage(this, getString(R.string.failed))
                }
            }

    }

    private fun updateCandidate(value: Int) {
        showProgressDialog(getString(R.string.voting))
        loading = true


        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()

        db!!.collection(Constants.COLLECTION_CANDIDATE)
            .document(candidate.id)
            .update(Constants.CANDIDATE_VOTE, FieldValue.increment(value.toLong()))
            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                loading = false
                if (reg_task.isSuccessful) {

                    if (value == 1) {
                        updateConstituency()
                    } else {
                        updateVoter("0")
                    }
                } else {

                    updateVoter("0")
                    showMessage(this, getString(R.string.failed))
                }
            }


    }

    private fun updateConstituency() {
        showProgressDialog(getString(R.string.voting))
        loading = true
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        db!!.collection(Constants.COLLECTION_CONSTITUENCY)
            .document(mVoterModel.constituencyid)
            .update(Constants.CONSTITUENCY_CAST_VOTE, FieldValue.increment(1))

            .addOnCompleteListener { reg_task ->
                dismissProgressDialogPopup()
                loading = false
                if (reg_task.isSuccessful) {
                    sendSMS(mVoterModel)
                    showDoneMessage()
                } else {
                    updateCandidate(-1)
                    showMessage(this, getString(R.string.failed))
                }
            }

    }


    private fun showDoneMessage() {

        fireBroadcaste()
        val infoDialog: Dialog? = createCustomDialog(
            this@EnterCandidateActivity, R.layout.layout_message,
            R.style.PopDialogAnimation, true, cancelable = true, isBottom = true
        )
        infoDialog!!.findViewById<TextView>(R.id.tv_message).text =
            getString(R.string.action_excecuted_sucess)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).text = getString(R.string.ok)
        infoDialog.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            infoDialog.dismiss()
            finish()
        }
        infoDialog.show()
    }

    private fun sendSMS(user: VoterModel) {
        val apiServices = APIClient().client.create(ApiInterface::class.java)
        val message = StringBuilder()
        message.append("Tank you ")
        message.append(user.name)
        message.append(" for casting you vote.")

        val call = apiServices.sendSMS(message.toString(), "TXTLCL", user.mobile)

        call.enqueue(object : Callback<SmsResponseModel> {
            override fun onResponse(
                call: Call<SmsResponseModel>,
                response: Response<SmsResponseModel>
            ) {
                showMessage(this@EnterCandidateActivity, response.message())
            }

            override fun onFailure(call: Call<SmsResponseModel>?, t: Throwable?) {
                showMessage(this@EnterCandidateActivity, t!!.localizedMessage)
            }
        })
    }
    private fun fireBroadcaste() {
        val localBroadcastManager: LocalBroadcastManager =
            LocalBroadcastManager.getInstance(this)
        val localIntent = Intent(Constants.ACTION_BROADCAST_FCM_RECIEVED)
        localBroadcastManager.sendBroadcast(localIntent)
    }

    private fun getIntentData() {
        if ((intent.extras != null && intent.extras!!.getSerializable("constituencydata") != null)
            && (intent.extras != null && intent.extras!!.getSerializable("voterdata") != null)
        ) {
            mConstituencyModel =
                (intent.extras!!.getSerializable("constituencydata") as ConstituencyModel?)!!
            mVoterModel =
                (intent.extras!!.getSerializable("voterdata") as VoterModel?)!!

            if (mConstituencyModel.name != "" && mVoterModel.name != "") {

                getParty(mVoterModel.stateid)
            }

        }
    }

    private fun getParty(stateId: String) {


        showProgressDialog(getString(R.string.loading))
        mPartyDataList.clear()
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_PARTY)
        val query = ref
            .whereEqualTo(Constants.STATE_ID, stateId)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                val mList: java.util.ArrayList<PartyModel> = arrayListOf()
                for (document in fetchall_task.documents) {
                    Log.d(" data", document.id + " => " + document.data)
                    val mCategoryData =
                        PartyModel()
                    mCategoryData.id = document.id
                    mCategoryData.image =
                        (document.data?.get(Constants.PARTY_IMAGE)).toString()
                    mCategoryData.name =
                        (document.data?.get(Constants.PARTY_NAME)).toString()
                    mCategoryData.stateid =
                        (document.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (document.data?.get(Constants.STATE_NAME)).toString()
                    mCategoryData.isRegional =
                        (document.data?.get(Constants.PARTY_IS_REGIONAL)).toString()
                    mList.add(mCategoryData)


                }
                mPartyDataList.addAll(mList)

            }
            .addOnCompleteListener {
                loading = false
                dismissProgressDialogPopup()
                getCandidates(mVoterModel.constituencyid)
            }

    }

    private fun getCandidates(constituencyID: String) {

        if (mPartyDataList.isNotEmpty()) {
            loading = true
            mCandidateList.clear()
            showProgressDialog(getString(R.string.loading))
            val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
            val ref = db!!.collection(Constants.COLLECTION_CANDIDATE)
            val query = ref
                .whereEqualTo(Constants.CONSTITUENCY_ID, constituencyID)

            query.get()
                .addOnSuccessListener { fetchall_task ->

                    val mList: ArrayList<CandidateModel> = arrayListOf()
                    for (document in fetchall_task.documents) {
                        Log.d(" data", document.id + " => " + document.data)
                        val mCategoryData =
                            CandidateModel()
                        mCategoryData.id = document.id
                        mCategoryData.image =
                            (document.data?.get(Constants.CANDIDATE_IMAGE)).toString()
                        mCategoryData.name =
                            (document.data?.get(Constants.CANDIDATE_NAME)).toString()
                        mCategoryData.logo =
                            (document.data?.get(Constants.LOGO)).toString()
                        mCategoryData.state = ""
                        mCategoryData.stateid =
                            (document.data?.get(Constants.STATE_ID)).toString()
                        mCategoryData.constituency = ""
                        mCategoryData.constituencyid =
                            (document.data?.get(Constants.CONSTITUENCY_ID)).toString()
                        mCategoryData.partyid =
                            (document.data?.get(Constants.PARTY_ID)).toString()
                        var partyname = ""
                        if (mPartyDataList.filter { it.id == mCategoryData.partyid }.isNotEmpty()) {
                            partyname =
                                mPartyDataList.filter { it.id == mCategoryData.partyid }.single()
                                    .name
                        }
                        mCategoryData.party = partyname
                        var partylogo = ""
                        if (mPartyDataList.filter { it.id == mCategoryData.partyid }.isNotEmpty()) {
                            partylogo =
                                mPartyDataList.filter { it.id == mCategoryData.partyid }.single()
                                    .image
                        }

                        mCategoryData.partyimage = partylogo
                        mList.add(mCategoryData)


                    }
                    mCandidateList.clear()
                    mCandidateList.addAll(mList)
                    mDisplayedList.clear()
                    for (candidate in mCandidateList) {
                        mDisplayedList.add(candidate)
                    }

                    mAdapter.notifyDataSetChanged()
                    if (mCandidateList.isEmpty()) {
                        tvNodata.visibility = View.VISIBLE
                    } else {
                        tvNodata.visibility = View.GONE
                    }

                }
                .addOnCompleteListener {
                    loading = false
                    dismissProgressDialogPopup()
                }
        } else {

        }


    }


    override fun onStop() {
        super.onStop()
    }

    override fun onItemClick(position: Int) {
        mDisplayedList.clear()
        for (candidate in mCandidateList) {
            val can = candidate
            can.mIsSelected = mCandidateList[position].id == can.id
            mDisplayedList.add(can)
        }
        mAdapter.notifyDataSetChanged()
    }

    override fun onImageClick(position: Int) {
    }

    override fun onItemRemoveClick(position: Int) {
    }

}
