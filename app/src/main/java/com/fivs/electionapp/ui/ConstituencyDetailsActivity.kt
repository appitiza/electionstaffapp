package com.fivs.electionapp.ui

import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.webkit.WebView
import com.fivs.electionapp.R
import com.fivs.electionapp.model.ConstituencyModel
import com.fivs.electionapp.model.StaffModel
import com.fivs.electionapp.utils.Constants
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_constituency_details.*


class ConstituencyDetailsActivity : BaseActivity() {

    private var mConstituencyModel = ConstituencyModel()
    private var mStaffModel = StaffModel()
    private var mWebView: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_constituency_details)
        setScreenTitle(R.string.app_name)
        getIntentData()
        initializeGraph()

    }


    private fun getIntentData() {
        if (intent.extras != null && intent.extras!!.getSerializable("staffdata") != null) {
            mStaffModel = (intent.extras!!.getSerializable("staffdata") as StaffModel?)!!
            getConstituencyInfo(mStaffModel.constituencyid)

        }
    }

    private fun getConstituencyInfo(id: String) {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_CONSTITUENCY)
        val query = ref
            .document(id)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if (fetchall_task != null) {
                    val mCategoryData =
                        ConstituencyModel()
                    mCategoryData.id = fetchall_task.id
                    mCategoryData.image =
                        (fetchall_task?.get(Constants.CONSTITUENCY_IMAGE)).toString()
                    mCategoryData.name =
                        (fetchall_task.get(Constants.CONSTITUENCY_NAME)).toString()
                    mCategoryData.isInactive =
                        (fetchall_task.get(Constants.CONSTITUENCY_STATUS)).toString()

                    mCategoryData.cast_vote =
                        (fetchall_task.get(Constants.CONSTITUENCY_CAST_VOTE)).toString().toInt()
                    mCategoryData.voter_count =
                        (fetchall_task.get(Constants.CONSTITUENCY_VOTER_COUNT)).toString().toInt()
                    mCategoryData.won_candidate =
                        (fetchall_task.get(Constants.CONSTITUENCY_WON_CANDIDATE)).toString()

                    mCategoryData.stateid =
                        (fetchall_task.get(Constants.STATE_ID)).toString()
                    mCategoryData.state = ""

                    mConstituencyModel = mCategoryData


                    val displayString = StringBuilder()


                    displayString.append("Total Voter Count: ")
                    displayString.append(mConstituencyModel.voter_count)
                    displayString.append("\n")
                    displayString.append("Done: ")
                    displayString.append(mConstituencyModel.cast_vote)
                    displayString.append("\n")
                    displayString.append("Pending: ")
                    displayString.append(mConstituencyModel.voter_count - mConstituencyModel.cast_vote)

                    tvInfo.text = displayString.toString()

                    setData(mConstituencyModel.cast_vote, mConstituencyModel.voter_count)
                }

            }
            .addOnCompleteListener {
                dismissProgressDialogPopup()
            }

    }

    private fun setData(done: Int, total: Int) {
        val entries: ArrayList<PieEntry> = ArrayList()

        entries.add(PieEntry(done.toFloat(), "DONE"))
        entries.add(PieEntry((total - done).toFloat(), "PENDING"))

        val dataSet = PieDataSet(entries, "Election Results")
        dataSet.setDrawIcons(false)
        dataSet.sliceSpace = 3f
        dataSet.iconsOffset = MPPointF(0f, 40f)
        dataSet.selectionShift = 5f
        // add a lot of colors
        val colors: ArrayList<Int> = ArrayList()
        for (c in ColorTemplate.VORDIPLOM_COLORS) colors.add(c)
        for (c in ColorTemplate.PASTEL_COLORS) colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
        dataSet.colors = colors
        //dataSet.setSelectionShift(0f);
        val data = PieData(dataSet)
        data.setValueFormatter(PercentFormatter(chart))
        data.setValueTextSize(16f)
        data.setValueTextColor(Color.BLUE)
        // data.setValueTypeface(tfLight)
        chart.data = data
        // undo all highlights
        chart.highlightValues(null)
        chart.invalidate()
    }

    private fun initializeGraph() {
        chart.setUsePercentValues(true)
        chart.description.isEnabled = false
        chart.setExtraOffsets(5f, 10f, 5f, 5f)

        chart.dragDecelerationFrictionCoef = 0.95f

        chart.centerText = generateCenterSpannableText()

        chart.isDrawHoleEnabled = true
        chart.setHoleColor(Color.WHITE)

        chart.setTransparentCircleColor(Color.WHITE)
        chart.setTransparentCircleAlpha(110)

        chart.holeRadius = 58f
        chart.transparentCircleRadius = 61f

        chart.setDrawCenterText(true)

        chart.setRotationAngle(0f)
        chart.isRotationEnabled = true
        chart.isHighlightPerTapEnabled = true




        chart.animateY(1400, Easing.EaseInOutQuad)
        val l = chart.legend
        l.verticalAlignment = Legend.LegendVerticalAlignment.TOP
        l.horizontalAlignment = Legend.LegendHorizontalAlignment.RIGHT
        l.orientation = Legend.LegendOrientation.VERTICAL
        l.setDrawInside(false)
        l.xEntrySpace = 7f
        l.yEntrySpace = 0f
        l.yOffset = 0f

        chart.setEntryLabelColor(Color.BLUE)
        chart.setEntryLabelTextSize(12f)
    }

    private fun generateCenterSpannableText(): SpannableString? {
        val s = SpannableString("FIVS\ndeveloped by FIVS")
        return s
    }

}
