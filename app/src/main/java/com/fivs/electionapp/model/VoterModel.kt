package com.fivs.electionapp.model

import java.io.Serializable

open class VoterModel:Serializable {
    var id : String = ""
    var fingerimage : String = ""
    var name : String = ""
    var age : String = ""
    var mobile : String = ""
    var sex : String = ""
    var state : String = ""
    var stateid : String = ""
    var constituency : String = ""
    var constituencyid : String = ""
    var castvote : String = ""
    var enrolledTemplete : ByteArray = byteArrayOf()
}