package com.fivs.electionapp.ui

import android.animation.ObjectAnimator
import android.app.Activity
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.os.SystemClock
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.animation.addListener
import com.fivs.electionapp.R
import com.fivs.electionapp.model.StaffModel
import com.fivs.electionapp.model.VoterModel
import com.mantra.mfs100.FingerData
import com.mantra.mfs100.MFS100
import com.mantra.mfs100.MFS100Event
import kotlinx.android.synthetic.main.activity_finger_scanner.*
import java.lang.IllegalStateException


class VoterFingerScannerActivity : AppCompatActivity(), MFS100Event {
    private var countDownTimer: CountDownTimer? = null

    private var status = 0
    private var attempt = 1
    private val startTime = 5 * 1000.toLong()
    private val interval = 1 * 1000.toLong()

    private var timeout = 20000
    private var mfs100: MFS100? = null

    private var isCaptureRunning = false
    private var mLastAttTime: Long = 0
    private var mLastDttTime = 0L

    private var mLastClkTime: Long = 0
    private val Threshold: Long = 1500

    private var mMediaPlayer: MediaPlayer = MediaPlayer()
    private lateinit var mAnimator: ObjectAnimator

    private lateinit var Enroll_Template: ByteArray
    private lateinit var Verify_Template: ByteArray
    private var mModel = VoterModel()
    private var isPageAlive = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_finger_scanner)
        initializeScannerComponents()
        setClick()
    }

    private fun setClick() {
        btnTryAgain.setOnClickListener {
            if (attempt < 3) {
                if (SystemClock.elapsedRealtime() - mLastClkTime < Threshold) {
                    return@setOnClickListener
                }
                mLastClkTime = SystemClock.elapsedRealtime()

                attempt++
                tvMessage.text = ""
                initializeTimmer()
            } else {
                btnTryAgain.visibility = View.GONE
                tvMessage.text = getString(R.string.exceeded_the_limit)
                playWarnig()
            }
        }

    }

    private fun initializeScannerComponents() {
        tvMessage.text = getString(R.string.processing)
        try {
            this.window
                .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
            mfs100 = MFS100(this)
            mfs100!!.SetApplicationContext(this@VoterFingerScannerActivity)
        } catch (e: Exception) {
            Log.e("Error", e.toString())
        }


        if (intent.extras != null && intent.extras!!.getSerializable("data") != null) {
            mModel = (intent.extras!!.getSerializable("data") as VoterModel?)!!
        }
    }

    override fun onStart() {
        isPageAlive = true
        try {
            if (mfs100 == null) {
                mfs100 = MFS100(this)
                mfs100!!.SetApplicationContext(this@VoterFingerScannerActivity)
                InitScanner()
            } else {
                InitScanner()
            }

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        super.onStart()
    }

    override fun onStop() {
        isPageAlive = false
        try {
            if (countDownTimer != null) {
                countDownTimer!!.cancel()
            }
            stopScannerAnimation()
            stopSound()
            if (isCaptureRunning) {
                mfs100!!.StopAutoCapture()
            }
            Thread.sleep(500)
            finish()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        super.onStop()
    }

    private fun InitScanner() {
        try {
            val ret = mfs100!!.Init()
            if (ret != 0) {
                tvMessage.text = (mfs100!!.GetErrorMsg(ret))
            } else {
                tvMessage.text = getString(R.string.scanner_initiation_success)
                val info = ("Serial: " + mfs100!!.GetDeviceInfo().SerialNo()
                        + " Make: " + mfs100!!.GetDeviceInfo().Make()
                        + " Model: " + mfs100!!.GetDeviceInfo().Model()
                        + "\nCertificate: " + mfs100!!.GetCertification())
                tvMessage.text = info
                initializeTimmer()
            }
        } catch (ex: java.lang.Exception) {

            tvMessage.text = getString(R.string.scanner_initiation_error)
        }
    }

    private fun StartSyncCapture() {
        Thread(Runnable {
            isCaptureRunning = true
            try {

                val fingerData = FingerData()
                val ret =
                    mfs100!!.AutoCapture(fingerData, timeout, true)
                if (ret != 0) {
                    this@VoterFingerScannerActivity.runOnUiThread {
                        stopScannerAnimation()
                        stopSound()
                        btnTryAgain.visibility = View.VISIBLE
                        tvMessage.text = (mfs100!!.GetErrorMsg(ret))
                    }
                } else {


                    val bitmap = BitmapFactory.decodeByteArray(
                        fingerData.FingerImage(), 0,
                        fingerData.FingerImage().size
                    )
                    this@VoterFingerScannerActivity.runOnUiThread {
                        stopScannerAnimation()
                        stopSound()
                        btnTryAgain.visibility = View.GONE
                        ivFinger.setImageBitmap(bitmap)
                        //compare
                        processingData(fingerData)
                    }


                }
            } catch (ex: java.lang.Exception) {
                tvMessage.text = ex.message.toString()
                btnTryAgain.visibility = View.VISIBLE
                stopScannerAnimation()
                stopSound()
            } finally {
                isCaptureRunning = false
            }
        }).start()
    }

    private fun processingData(fingerData: FingerData) {
        try {
            Enroll_Template = ByteArray(fingerData.ISOTemplate().size)
            System.arraycopy(
                fingerData.ISOTemplate(), 0, Enroll_Template, 0,
                fingerData.ISOTemplate().size
            )
            val ret = mfs100!!.MatchISO(Enroll_Template, mModel.enrolledTemplete)
            if (ret < 0) {
                tvMessage.text = mfs100!!.GetErrorMsg(ret)

            } else {
                if (ret >= 96) {
                    tvMessage.text = getString(R.string.finger_matches_with, mModel.name)
                    this@VoterFingerScannerActivity.runOnUiThread {
                        val intent = Intent()
                        val mBundle = Bundle()
                        mBundle.putString("verification", "1")
                        intent.putExtras(mBundle)
                        setResult(Activity.RESULT_OK, intent)
                        finish()
                    }
                } else {

                    btnTryAgain.visibility = View.GONE
                    tvMessage.text = getString(R.string.finger_donot_matches_with, mModel.name)
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

    }

    private fun initializeTimmer() {

        status = 0
        btnTryAgain.visibility = View.GONE
        pbCounter.visibility = View.VISIBLE
        pbCounter.setProgress(100)
        playScan()
        countDownTimer = object : CountDownTimer(startTime, interval) {
            override fun onTick(millisUntilFinished: Long) {
                status++
                pbCounter.setProgress((100 - (status * 20)))
            }

            override fun onFinish() {
                if (isPageAlive) {
                    pbCounter.setProgress(0)
                    pbCounter.visibility = View.GONE

                    val ret = mfs100!!.Init()
                    if (ret == 0) {
                        //auto start
                        if (!isCaptureRunning) {
                            StartSyncCapture()
                            triggerDownScannerAnimation()
                            playScanning()

                        }
                    }
                }
            }
        }
        countDownTimer?.start()
    }

    private fun triggerDownScannerAnimation() {
        scannerBar.visibility = View.VISIBLE
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        if (::mAnimator.isInitialized) {
            mAnimator.removeAllListeners()
            mAnimator.cancel()
            mAnimator.end()
            mAnimator = ObjectAnimator.ofFloat(scannerBar, "translationY", height.toFloat() - 100)
        } else {

            mAnimator = ObjectAnimator.ofFloat(scannerBar, "translationY", height.toFloat() - 100)
        }


        mAnimator.apply {

            duration = 2000
            startDelay = 200
            mAnimator.addListener(onEnd = {
                triggerUpScannerAnimation()
            })
            AccelerateDecelerateInterpolator()
            start()
        }

    }

    private fun triggerUpScannerAnimation() {
        scannerBar.visibility = View.VISIBLE
        if (::mAnimator.isInitialized) {
            mAnimator.removeAllListeners()
            mAnimator.cancel()
            mAnimator.end()
            mAnimator = ObjectAnimator.ofFloat(scannerBar, "translationY", 0F)
        } else {

            mAnimator = ObjectAnimator.ofFloat(scannerBar, "translationY", 0F)
        }
        mAnimator.apply {
            duration = 2000
            startDelay = 200
            mAnimator.addListener(onEnd = {
                triggerDownScannerAnimation()
            })
            AccelerateDecelerateInterpolator()
            start()
        }
    }

    private fun stopScannerAnimation() {
        if (::mAnimator.isInitialized) {
            if (mAnimator != null) {
                mAnimator.removeAllListeners()
                mAnimator.cancel()
                mAnimator.end()
            }
        }
        scannerBar.visibility = View.GONE
    }

    private fun stopSound() {
        try {
            if (mMediaPlayer != null) {
                if (mMediaPlayer.isPlaying) {
                    mMediaPlayer.pause()
                    mMediaPlayer.stop()
                    mMediaPlayer.release()
                }

            }
        }
        catch (e: IllegalStateException)
        {

        }
    }

    private fun playWarnig() {

        mMediaPlayer =
            MediaPlayer.create(this, R.raw.warning)

        mMediaPlayer.start()
    }

    private fun playScan() {

        mMediaPlayer =
            MediaPlayer.create(this, R.raw.scan)

        mMediaPlayer.start()
    }

    private fun playScanning() {
        /*if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying) {
                mMediaPlayer.pause()
            }
            mMediaPlayer.stop()
            mMediaPlayer.release()
        }*/
        mMediaPlayer =
            MediaPlayer.create(this, R.raw.scanning)

        mMediaPlayer.start()

        mMediaPlayer.isLooping = true

    }

    override fun OnDeviceAttached(vid: Int, pid: Int, hasPermission: Boolean) {
        if (SystemClock.elapsedRealtime() - mLastAttTime < Threshold) {
            tvMessage.text = getString(R.string.rejected_time_boundry)
            return
        }
        mLastAttTime = SystemClock.elapsedRealtime()
        val ret: Int
        if (!hasPermission) {
            tvMessage.text = getString(R.string.permission_denied)
            return
        }
        try {
            if (vid == 1204 || vid == 11279) {
                if (pid == 34323) {
                    ret = mfs100!!.LoadFirmware()
                    if (ret != 0) {
                        tvMessage.text = mfs100!!.GetErrorMsg(ret)
                    } else {
                        initializeTimmer()
                        tvMessage.text = getString(R.string.firmware_loaded)
                    }
                } else if (pid == 4101) {
                    ret = mfs100!!.Init()
                    if (ret != 0) {
                        tvMessage.text = mfs100!!.GetErrorMsg(ret)
                    } else {
                        initializeTimmer()
                        tvMessage.text = getString(R.string.firmware_loaded)
                    }
                }
            } else {
                tvMessage.text = getString(R.string.firmware_failed_connect)
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun OnDeviceDetached() {
        stopScannerAnimation()
        stopSound()
        try {
            if (SystemClock.elapsedRealtime() - mLastDttTime < Threshold) {
                return
            }
            mLastDttTime = SystemClock.elapsedRealtime()
            try {
                val ret = mfs100!!.UnInit()
                if (ret != 0) {
                    tvMessage.text = mfs100!!.GetErrorMsg(ret)
                } else {
                    tvMessage.text = getString(R.string.firmware_disconnected)
                }
            } catch (e: java.lang.Exception) {
                Log.e("UnInitScanner.EX", e.toString())
            }
            tvMessage.text = getString(R.string.firmware_removed)
        } catch (e: java.lang.Exception) {
        }
    }

    override fun OnHostCheckFailed(p0: String?) {
        try {
            tvMessage.text = p0
            Toast.makeText(applicationContext, p0, Toast.LENGTH_LONG).show()
        } catch (ignored: java.lang.Exception) {
        }
    }

}
