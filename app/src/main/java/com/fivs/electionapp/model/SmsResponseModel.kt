package com.fivs.electionapp.model

data class SmsResponseModel(
    var balance: Int,
    var batch_id: Int,
    var cost: Int,
    var custom: String,
    var message: Message,
    var messages: List<Messages>,
    var num_messages: Int,
    var receipt_url: String,
    var status: String
) {
    data class Message(
        var content: String,
        var num_parts: Int,
        var sender: String
    )

    data class Messages(
        var id: String,
        var recipient: Long
    )
}