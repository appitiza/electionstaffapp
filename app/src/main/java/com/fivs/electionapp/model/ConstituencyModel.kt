package com.fivs.electionapp.model

import java.io.Serializable

open class ConstituencyModel:Serializable {
    var id : String = ""
    var image : String = ""
    var name : String = ""
    var isInactive : String = "0"
    var cast_vote : Int = 0
    var voter_count : Int = 0
    var won_candidate : String = ""
    var state : String = ""
    var stateid : String = ""

}