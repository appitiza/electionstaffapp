package com.fivs.electionapp.network

import com.fivs.electionapp.model.SmsResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiInterface {
    @GET("send/")
    fun sendSMS(
        @Query("message") message: String,
        @Query("sender") sender: String,
        @Query("numbers") numbers: String,
        @Query("apikey") apikey: String = "SSZYgCp5Xp4-oBDOMtC9n8o9DlgMkHNhCmO1SFCoLk"
    ): Call<SmsResponseModel>
}