package com.fivs.electionapp.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import com.fivs.electionapp.R
import com.fivs.electionapp.model.SmsResponseModel
import com.fivs.electionapp.model.StaffModel
import com.fivs.electionapp.network.APIClient
import com.fivs.electionapp.network.ApiInterface
import com.fivs.electionapp.utils.Constants
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.Blob
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class LoginActivity : BaseActivity() {
    private var mAuth: FirebaseAuth? = null
    private var SCAN_FINGER: Int = 4
    private var mStaffModel = StaffModel()
    private var isProcessing = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initialize()
        setClick()
    }

    fun initialize() {
        mAuth = FirebaseAuth.getInstance()
        hideToolbar()
    }

    fun setClick() {
        btnLogin.setOnClickListener {
            if (isNetworkAvailable()) {
                if (etEmail.text.toString().trim().isNotEmpty() && etPassword.text.toString().trim().isNotEmpty()) {

                    performLogin()


                } else {
                    showMessage(this, getString(R.string.fill_all))
                }
            } else {
                showMessage(this, getString(R.string.no_network))
            }

        }
        tvForgotPassword.setOnClickListener {
            if (isNetworkAvailable()) {
                if (etEmail.text.toString().trim().isNotEmpty()) {
                    performReset()
                } else {
                    showMessage(this, getString(R.string.fill_all))
                }
            } else {
                showMessage(this, getString(R.string.no_network))
            }
        }
    }

    fun performLogin() {
        showProgressDialog(getString(R.string.loading))
        mAuth?.signInWithEmailAndPassword(
            etEmail.text.toString().trim(),
            etPassword.text.toString().trim()
        )
            ?.addOnCompleteListener(this) { task ->

                dismissProgressDialogPopup()
                if (task.isSuccessful) {
                    if (isNetworkAvailable()) {
                        getStaffInfo(mAuth!!.currentUser!!.email.toString())
                    } else {
                        showMessage(this, getString(R.string.no_network))
                    }
                } else {
                    showMessage(this, task.exception?.message.toString())

                }
            }
    }

    fun performReset() {
        mAuth?.sendPasswordResetEmail(etEmail.text.toString().trim())
            ?.addOnCompleteListener(this) { task ->

                dismissProgressDialog()
                if (task.isSuccessful) {
                    showMessage(this, getString(R.string.password_reset_sent))

                } else {
                    showMessage(this, task.exception?.message.toString())
                }

            }
    }

    fun print() {
        /* try {
                 val policy =
                     StrictMode.ThreadPolicy.Builder().permitAll().build()
                 StrictMode.setThreadPolicy(policy)
                 val sock = Socket("192.168.200.114", 9100)
                 val oStream = PrintWriter(sock.getOutputStream())
                 oStream.println(getHtmlContent())
                 oStream.println("\n\n\n")
                 oStream.close()
                 sock.close()
             } catch (e: UnknownHostException) {
                 e.printStackTrace()
             } catch (e: IOException) {
                 e.printStackTrace()
             }*/
    }

    private fun getStaffInfo(id: String) {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_STAFF)
        val query = ref
            .document(id)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if (fetchall_task != null) {
                    val mCategoryData =
                        StaffModel()
                    mCategoryData.id = fetchall_task.id
                    mCategoryData.fingerimage =
                        (fetchall_task.data?.get(Constants.STAFF_FINGER_IMAGE)).toString()
                    mCategoryData.name =
                        (fetchall_task.data?.get(Constants.STAFF_NAME)).toString()
                    mCategoryData.mobile =
                        (fetchall_task.data?.get(Constants.STAFF_MOBILE)).toString()
                    mCategoryData.stateid =
                        (fetchall_task.data?.get(Constants.STATE_ID)).toString()
                    mCategoryData.constituencyid =
                        (fetchall_task.data?.get(Constants.CONSTITUENCY_ID)).toString()
                    mCategoryData.enrolledTemplete =
                        ((fetchall_task.data?.get(Constants.STAFF_FINGER_TEMPLETE)) as Blob).toBytes()

                    mStaffModel = mCategoryData


                    getElectionTime()
                }

            }
            .addOnCompleteListener {
                dismissProgressDialogPopup()
            }

    }

    fun getElectionTime() {

        isProcessing = true
        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val calendar = Calendar.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_ELECTION_TIME)
        val query = ref
            .document(Constants.ELECTION_TIME_KEY)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if ((fetchall_task.data?.get(Constants.ELECTION_START_TIME)).toString() != "null") {
                    val parser =
                        SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyy", Locale.getDefault())
                    val formatter = SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.getDefault())


                    val start =
                        parser.parse(fetchall_task.data?.get(Constants.ELECTION_START_TIME).toString())
                    val end =
                        parser.parse(fetchall_task.data?.get(Constants.ELECTION_END_TIME).toString())
                    if (start!!.before(calendar.time) && end!!.after(calendar.time)) {
                        val intent = Intent(this, FingerScannerActivity::class.java)
                        val mBundle = Bundle()
                        mBundle.putSerializable("data", mStaffModel)
                        intent.putExtras(mBundle)
                        startActivityForResult(intent, SCAN_FINGER)

                        /*val intent = Intent(this, MainActivity::class.java)
                        val mBundle = Bundle()
                        mBundle.putSerializable("staffdata", mStaffModel)
                        intent.putExtras(mBundle)
                        startActivity(intent)*/
                    } else {
                        showMessage(this, getString(R.string.election_not_started))
                    }

                } else {
                    showMessage(this, getString(R.string.election_not_started))
                }

                isProcessing = false
                dismissProgressDialogPopup()

            }
            .addOnFailureListener {
                isProcessing = false
                dismissProgressDialogPopup()
            }
    }


    private fun sendSMS(user: StaffModel) {
        val apiServices = APIClient().client.create(ApiInterface::class.java)
        val message = StringBuilder()
        message.append(user.name)
        message.append(" Tried To Login")

        val call = apiServices.sendSMS(message.toString(), "TXTLCL", user.mobile)

        call.enqueue(object : Callback<SmsResponseModel> {
            override fun onResponse(
                call: Call<SmsResponseModel>,
                response: Response<SmsResponseModel>
            ) {

            }

            override fun onFailure(call: Call<SmsResponseModel>?, t: Throwable?) {
            }
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SCAN_FINGER && resultCode == Activity.RESULT_OK) {

            if (data != null) {
                val verification = data.getStringExtra("verification")
                if (verification != null) {
                    if (verification == "1") {
                        sendSMS(mStaffModel)

                        val intent = Intent(this, MainActivity::class.java)
                        val mBundle = Bundle()
                        mBundle.putSerializable("staffdata", mStaffModel)
                        intent.putExtras(mBundle)
                        startActivity(intent)


                    } else {
                        Toast.makeText(this, "verification failed", Toast.LENGTH_LONG).show()
                    }

                }

            } else {
                Toast.makeText(this, "Data is null", Toast.LENGTH_LONG).show()
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}
