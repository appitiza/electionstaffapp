package com.fivs.electionapp.ui

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.media.MediaPlayer
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.webkit.WebView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.fivs.electionapp.R
import com.fivs.electionapp.model.ConstituencyModel
import com.fivs.electionapp.model.StaffModel
import com.fivs.electionapp.model.VoterModel
import com.fivs.electionapp.utils.Constants
import com.google.firebase.firestore.Blob
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_enter_adhaar.*


class EnterAdhaarActivity : BaseActivity() {


    private var SCAN_FINGER: Int = 4
    private var mStaffModel = StaffModel()
    private var mConstituencyModel = ConstituencyModel()
    private var mVoterModel = VoterModel()
    private var mWebView: WebView? = null
    private var mMediaPlayer: MediaPlayer = MediaPlayer()
    private var selection_state = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_adhaar)
        setScreenTitle(R.string.app_name)
        getIntentData()
        setClick()

    }

    private fun setClick() {
        btnProceed.setOnClickListener {
            validateAdhar()
        }
        etAdhaar.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(p0!!.length >= 12) {
                    validateAdhar()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
    }

    private fun validateAdhar() {
        if (isNetworkAvailable()) {
            if (etAdhaar.text.toString().trim().isNotEmpty()) {
                if (mStaffModel.name != "" && mConstituencyModel.name != "") {
                    if (selection_state == 1) {
                        getVoterInfo(etAdhaar.text.toString().trim())
                    } else {
                        if (mVoterModel.name != "") {
                             val intent = Intent(this, VoterFingerScannerActivity::class.java)
                             val mBundle = Bundle()
                             mBundle.putSerializable("data", mVoterModel)
                             intent.putExtras(mBundle)
                             startActivityForResult(intent, SCAN_FINGER)

                           /* val intent = Intent(this, EnterCandidateActivity::class.java)
                            val mBundle = Bundle()
                            mBundle.putSerializable("constituencydata", mConstituencyModel)
                            mBundle.putSerializable("voterdata", mVoterModel)
                            intent.putExtras(mBundle)
                            startActivity(intent)*/
                        }

                    }
                }
            } else {
                showMessage(this, getString(R.string.fill_all))
            }
        } else {
            showMessage(this, getString(R.string.no_network))
        }
    }

    private fun getIntentData() {
        if (intent.extras != null && intent.extras!!.getSerializable("staffdata") != null) {
            mStaffModel = (intent.extras!!.getSerializable("staffdata") as StaffModel?)!!
            mConstituencyModel =
                (intent.extras!!.getSerializable("constituencydata") as ConstituencyModel?)!!

        }
    }

    private fun getVoterInfo(id: String) {

        showProgressDialog(getString(R.string.loading))
        val db: FirebaseFirestore? = FirebaseFirestore.getInstance()
        val ref = db!!.collection(Constants.COLLECTION_VOTER)
        val query = ref
            .document(id)

        query.get()
            .addOnSuccessListener { fetchall_task ->

                if (fetchall_task != null && (fetchall_task.get(Constants.VOTER_NAME)).toString() != "null") {

                    val mCategoryData =
                        VoterModel()
                    mCategoryData.id = fetchall_task.id
                    mCategoryData.name =
                        (fetchall_task.get(Constants.VOTER_NAME)).toString()
                    mCategoryData.age =
                        (fetchall_task.get(Constants.VOTER_AGE)).toString()
                    mCategoryData.mobile =
                        (fetchall_task.get(Constants.VOTER_MOBILE)).toString()
                    mCategoryData.sex =
                        (fetchall_task.get(Constants.VOTER_SEX)).toString()
                    mCategoryData.fingerimage =
                        (fetchall_task.get(Constants.VOTER_FINGER_IMAGE)).toString()
                    mCategoryData.castvote =
                        (fetchall_task.get(Constants.VOTER_CASTE_VOTE)).toString()
                    mCategoryData.stateid =
                        (fetchall_task.get(Constants.STATE_ID)).toString()
                    mCategoryData.state =
                        (fetchall_task.get(Constants.STATE_NAME)).toString()
                    mCategoryData.constituencyid =
                        (fetchall_task.get(Constants.CONSTITUENCY_ID)).toString()
                    mCategoryData.constituency =
                        (fetchall_task.get(Constants.CONSTITUENCY_NAME)).toString()
                    if (fetchall_task.get(Constants.VOTER_FINGER_TEMPLETE) != null) {
                        mCategoryData.enrolledTemplete =
                            ((fetchall_task.get(Constants.VOTER_FINGER_TEMPLETE)) as Blob).toBytes()



                        if (mCategoryData.name != "" && mConstituencyModel.name != "" && mCategoryData.constituencyid == mConstituencyModel.id) {
                            if (mCategoryData.castvote == "1") {
                                showMessage(this, getString(R.string.already_caste_vote))
                                playWarnig()
                            } else {
                                mVoterModel = mCategoryData
                                val displayString = StringBuilder()
                                displayString.append("Name: " + mVoterModel.name)
                                displayString.append("\n")
                                displayString.append("Age: " + mVoterModel.age)
                                displayString.append("\n")
                                displayString.append("Mobile: " + mVoterModel.mobile)
                                tvInfo.text = displayString.toString()
                                selection_state = 2


                            }
                        } else {
                            showMessage(this, getString(R.string.you_are_not_allowed))
                        }


                    }


                } else {
                    showMessage(this, getString(R.string.this_adhaar_not_enrolled))
                }

            }
            .addOnCompleteListener {
                dismissProgressDialogPopup()
            }

    }

    private fun playWarnig() {

        mMediaPlayer =
            MediaPlayer.create(this, R.raw.warning)

        mMediaPlayer.start()
    }

    private fun stopSound() {
        if (mMediaPlayer != null) {
            if (mMediaPlayer.isPlaying) {
                mMediaPlayer.pause()
                mMediaPlayer.stop()
                mMediaPlayer.release()
            }

        }
    }

    override fun onStop() {
        super.onStop()
        stopSound()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SCAN_FINGER && resultCode == Activity.RESULT_OK) {

            if (data != null) {
                val verification = data.getStringExtra("verification")
                if (verification != null) {
                    if (verification == "1") {
                        if (mStaffModel.name != "" && mConstituencyModel.name != "" && mVoterModel.constituencyid == mConstituencyModel.id) {


                            val intent = Intent(this, EnterCandidateActivity::class.java)
                            val mBundle = Bundle()
                            mBundle.putSerializable("constituencydata", mConstituencyModel)
                            mBundle.putSerializable("voterdata", mVoterModel)
                            intent.putExtras(mBundle)
                            startActivity(intent)
                        } else {
                            showMessage(this, getString(R.string.you_are_not_allowed))
                        }


                    } else {

                        showMessage(this, getString(R.string.failed))
                    }

                }

            } else {
                showMessage(this, getString(R.string.failed))
            }

        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                listenerDataChange,
                IntentFilter(Constants.ACTION_BROADCAST_FCM_RECIEVED)
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(listenerDataChange)
    }

    private val listenerDataChange = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.ACTION_BROADCAST_FCM_RECIEVED -> {

                    finish()

                }
            }
        }
    }
}
