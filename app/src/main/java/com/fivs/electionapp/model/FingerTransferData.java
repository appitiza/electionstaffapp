package com.fivs.electionapp.model;

import java.io.Serializable;

public class FingerTransferData implements Serializable {
    byte[] _FingerImage = null;
    int _Quality = 0;
    int _Nfiq = 0;
    byte[] _RawData = null;
    byte[] _ISOTemplate = null;
    double _InWidth = 0.0D;
    double _InHeight = 0.0D;
    double _InArea = 0.0D;
    double _Resolution = 0.0D;
    int _GrayScale = 0;
    int _Bpp = 0;
    double _WSQCompressRatio = 0.0D;
    String _WSQInfo = "";

    public byte[] get_FingerImage() {
        return _FingerImage;
    }

    public void set_FingerImage(byte[] _FingerImage) {
        this._FingerImage = _FingerImage;
    }

    public int get_Quality() {
        return _Quality;
    }

    public void set_Quality(int _Quality) {
        this._Quality = _Quality;
    }

    public int get_Nfiq() {
        return _Nfiq;
    }

    public void set_Nfiq(int _Nfiq) {
        this._Nfiq = _Nfiq;
    }

    public byte[] get_RawData() {
        return _RawData;
    }

    public void set_RawData(byte[] _RawData) {
        this._RawData = _RawData;
    }

    public byte[] get_ISOTemplate() {
        return _ISOTemplate;
    }

    public void set_ISOTemplate(byte[] _ISOTemplate) {
        this._ISOTemplate = _ISOTemplate;
    }

    public double get_InWidth() {
        return _InWidth;
    }

    public void set_InWidth(double _InWidth) {
        this._InWidth = _InWidth;
    }

    public double get_InHeight() {
        return _InHeight;
    }

    public void set_InHeight(double _InHeight) {
        this._InHeight = _InHeight;
    }

    public double get_InArea() {
        return _InArea;
    }

    public void set_InArea(double _InArea) {
        this._InArea = _InArea;
    }

    public double get_Resolution() {
        return _Resolution;
    }

    public void set_Resolution(double _Resolution) {
        this._Resolution = _Resolution;
    }

    public int get_GrayScale() {
        return _GrayScale;
    }

    public void set_GrayScale(int _GrayScale) {
        this._GrayScale = _GrayScale;
    }

    public int get_Bpp() {
        return _Bpp;
    }

    public void set_Bpp(int _Bpp) {
        this._Bpp = _Bpp;
    }

    public double get_WSQCompressRatio() {
        return _WSQCompressRatio;
    }

    public void set_WSQCompressRatio(double _WSQCompressRatio) {
        this._WSQCompressRatio = _WSQCompressRatio;
    }

    public String get_WSQInfo() {
        return _WSQInfo;
    }

    public void set_WSQInfo(String _WSQInfo) {
        this._WSQInfo = _WSQInfo;
    }
}
